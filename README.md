# DemoMarcTim
This GitRepository is only a Playground.

## Setup
We have to disable the ssl verification in oder to clone/pull/push.
```
export GIT_SSL_NO_VERIFY=1
#or
git config --global http.sslverify false
```

Then you can clone the reposiroty.
```
git clone https://gitlab.com/musdasch/demomarctim.git
cd demomarctim/
```

Set local user email and name.
```
git config user.email "[YOUR_EMAIL]"
git config user.name "[YOUR_NAME]"
```

## Info
A helpful link to read about GIT: `https://git-scm.com/book/en/v2`
The manual: `https://git-scm.com/docs`
All about .gitignore: `https://git-scm.com/docs/gitignore`
